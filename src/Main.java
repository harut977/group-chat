import service.ChatService;
import service.PersonGeneratorService;



public class Main {
    public static void main(String[] args) {

        ChatService chatService = new ChatService(new PersonGeneratorService());

        chatService.startChat();
    }
}
