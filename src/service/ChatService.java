package service;

import model.Person;

import java.util.List;

public class ChatService {

    private PersonGeneratorService generatorService;

    public ChatService(PersonGeneratorService generatorService) {
        this.generatorService = new PersonGeneratorService();
    }

    public void startChat() {

        List<Person> personList = generatorService.getPersons();
        Person person;

        int size = personList.size();
        int number = 1;

        for (int i = 4; i >= 0; i--) {
            person = personList.get(i);

            for (int j = 0; j <= size - 1; j++) {
                if (j != i) {
                    System.out.println("->" + " " + person.getName() + "(" + person.getType().getTypeName() + ")" + ": Ասում է ։ "
                            + person.getType().sayHello(personList.get(j)) + "(" + personList.get(j).getAge() + "):" +
                            " --> " + personList.get(j).getName() + "(" + personList.get(j).getType().getTypeName() + ")" + ": Ասում է ։ "
                            + personList.get(j).getType().sayHello(person) + " " + "("+ person.getAge() + ")");
                } else {
                    continue;
                }
                if (number == 4 || number == 8 || number == 12 || number == 16) {
                    System.out.println();
                }
                number++;
            }
        }
    }
}
