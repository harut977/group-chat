package service;

import type.*;
import model.Person;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class PersonGeneratorService {

    Random rand = new Random();

    private List<Person> personList = null;

    private int getRandomAge() {
        return rand.nextInt(20) + 20;
    }

    private String getRandomName() {
        String personNames[] = {"Ալեքսանդր", "Արամ",
                "Անահիտ", "Իրինա",
                "Նարինե", "Արմեն",
                "Հայկ", "Սիլվա",
                "Սանասար", "Բաղդասար"};
        return personNames[rand.nextInt(personNames.length - 1) + 1];
    }

    private PersonType getRandomType() {
        PersonType personTypes[] = {new PersonTypePositiveImpl(), new PersonTypeFoolImpl(), new PersonTypeRealistImpl()};
        int num = rand.nextInt(3) + 1;
        return personTypes[num - 1];
    }


    List<Person> getPersons() {

        Person person = null;
        personList = new ArrayList<>();

        while (personList.size() < 5) {

            String name = getRandomName();
            boolean ifExist = true;

            while (ifExist && !personList.isEmpty()) {

                for (int i = 0; i < personList.size(); i++) {

                    if (name.equals(personList.get(i).getName())) {
                        ifExist = true;
                        name = getRandomName();
                        break;
                    } else {
                        ifExist = false;
                    }
                }
            }
            person = new Person(name, getRandomAge(), getRandomType());

            personList.add(person);
        }
        return personList;
    }






}
