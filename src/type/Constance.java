package type;

public class Constance {

    static final int CONTROL_AGE = 30;
    static final String POSITIVE_HELLO = "Ողջույն ";
    static final String FOOL_HELLO = "Բարև ";
    static final String REALIST_HELLO_FIRST = "Ողջույն ";
    static final String REALIST_HELLO_SECOND = "Բարև ";
}
