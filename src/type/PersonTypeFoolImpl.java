package type;

import model.Person;

public class PersonTypeFoolImpl implements PersonType {

    private String typeName = "Հիմար";

    @Override
    public String getTypeName() {
        return typeName;
    }

    @Override
    public String sayHello(Person person) {
        return Constance.FOOL_HELLO + person.getName();
    }


}