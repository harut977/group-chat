package type;

import model.Person;

public interface PersonType {

    String sayHello(Person person);

    String getTypeName();

}
