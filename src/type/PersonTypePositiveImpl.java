package type;

import model.Person;

public class PersonTypePositiveImpl implements PersonType {

    private String typeName = "Պոզիտիվ";

    @Override
    public String getTypeName() {
        return typeName;
    }

    @Override
    public String sayHello(Person person) {
        return Constance.POSITIVE_HELLO + person.getName();
    }


}
