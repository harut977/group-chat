package type;

import model.Person;

public class PersonTypeRealistImpl implements PersonType {

    private String typeName = "Ռեալիստ";

    @Override
    public String getTypeName() {
        return typeName;
    }

    @Override
    public String sayHello(Person person) {
        if (person.getAge() <= Constance.CONTROL_AGE) {
            return Constance.REALIST_HELLO_SECOND + person.getName();
        } else {
            return Constance.REALIST_HELLO_FIRST + person.getName();
        }
    }
}